import logging
import os
import re
from argparse import Namespace

logging.basicConfig(
    level=os.getenv("LOG_LEVEL", "info").upper(),
    filename=f"logs/{__name__}.log",
    filemode='a'
)
logger = logging.getLogger(__name__)

class Host():
    def __init__(self, name: str, attributes: dict, args: Namespace):
        self.name = name
        self.attributes = attributes
        self.args = args

    @property
    def output(self) -> str:
        return f"{self.name[0:15]}\t{str(self.attributes.get(self.args.mac_address_field)).ljust(12, ' ')}\t{str(self.attributes.get(self.args.password_field))}"

    def parse_input(self, line: str):
        mac_search = re.search(r'^[a-f0-9]{12}$', line, re.IGNORECASE)
        logger.debug(f"Mac Search: {mac_search}")
        if mac_search is not None:
            logger.debug(f"Found: {mac_search.group(0)}")
            self.attributes[self.args.mac_address_field] = re.sub(r'([a-f0-9]{2})', '\\1' + self.args.mac_seperator, mac_search.group(0), flags=re.IGNORECASE).upper().strip(self.args.mac_seperator)

        password_search = re.search(r'^[a-z0-9]{' + str(self.args.password_length) + '}$', line, re.IGNORECASE)
        logger.debug(f"Password Search: {password_search}")
        if password_search is not None:
            self.attributes[self.args.password_field] = password_search.group(0)
