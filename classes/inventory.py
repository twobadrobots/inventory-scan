import logging
import os
from argparse import Namespace

import yaml
from classes.host import Host

logging.basicConfig(
    level=os.getenv("LOG_LEVEL", "info").upper(),
    filename=f"logs/{__name__}.log",
    filemode='a'
)
logger = logging.getLogger(__name__)

class Inventory:
    def __init__(self, args: Namespace):
        self.args = args

        with open(self.args.inventory, "r") as inventory_file:
            self.inventory = yaml.load(inventory_file, Loader=yaml.CLoader)
        logger.debug(self.inventory)

        self.hosts = []
        for group in self.inventory.values():
            self.hosts.extend([Host(n, d, self.args) for n, d in group.get("hosts", {}).items()])

    def __del__(self):
        with open(self.args.inventory, "w") as inventory_file:
            yaml.dump(self.inventory, inventory_file)

    def render(self, stdscr, host: Host | None = None):
        stdscr.clear()

        stdscr.addstr(0, 0, "Scan barcodes for each host and then press return to continue.")
        stdscr.addstr(2, 0, "Name\t\tMAC Address\t\tDefault Password")

        if host is not None:
            offset = 3
            current_index = self.hosts.index(host)

            logger.debug(f"Rendering: {max(0, current_index+1 - self.args.max_hosts)}:{current_index+1}")
            for index, host in enumerate(self.hosts[max(0, current_index+1 - self.args.max_hosts):current_index+1]):
                stdscr.addstr(index + offset, 0, host.output)

        stdscr.refresh()
