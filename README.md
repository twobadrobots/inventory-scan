# Inventory Scan

Commissioning a full rack of servers starts with inventory, this project lets you use a handheld barcode scanner to obtain each server's BMC mac address and the unique factory set password, it can then spit out an ansible compatible inventory.

<img src="images/supermicro-tag.png" height="200px">
<img src="images/supermicro-mainboard.png" height="200px">
<img src="images/barcode-scanner.jpg" height="200px">

## Install
```shell
$ pip install -r requirements.txt
```

## Usage

Create an inventory yaml file like the one in the examples directory listing all of your hosts, see [Ansible Inventory Guide](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#default-groups) for more information. Then run:



```shell
$ python scan.py

Scan barcodes for each host and then press return to continue.

Name            MAC Address             Default Password
site1-rack11234 AC:AC:AC:AC:AC:AC       asdasdasda

```


```shell
usage: scan.py [-h] [--inventory INVENTORY] [--mac-address-field MAC_ADDRESS_FIELD] [--password-field PASSWORD_FIELD] [--password-length PASSWORD_LENGTH]

Inventory Scan

options:
  -h, --help            show this help message and exit
  --inventory INVENTORY
                        the path to the inventory file
  --mac-address-field MAC_ADDRESS_FIELD
                        the inventory field of mac addresses
  --password-field PASSWORD_FIELD
                        the password field of mac addresses
  --password-length PASSWORD_LENGTH
                        the length of passwords
```