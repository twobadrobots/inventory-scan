import argparse
import curses
import logging
import os
import pathlib

from classes.inventory import Inventory

logging.basicConfig(
    level=os.getenv("LOG_LEVEL", "info").upper(),
    filename=f"logs/{__name__}.log",
    filemode='a'
)
logger = logging.getLogger(__name__)


def update_host(inventory: dict, host_name: str, key: str, value: str):
    for groups in inventory.values():
        for host, detail in groups.get("hosts", {}).items():
            if host == host_name:
                host[key] = value
                return

def UGETCHAR_(scr):
    h = scr.getch()
    if h == 3:
        raise KeyboardInterrupt
    if h == 26:
        raise EOFError
    return h

def get_line(stdscr):
    line = ""
    key = None
    while key != 10:
        key = UGETCHAR_(stdscr)
        line += chr(key)

    return line.strip()

def main(stdscr, args):
    stdscr.clear()
    stdscr.refresh()
    stdscr.getch()
    stdscr.scrollok(True)

    inventory = Inventory(args)
    inventory.render(stdscr)

    for host in inventory.hosts:
        inventory.render(stdscr, host)
        line = None
        while line is None or len(line) > 0:
            line = get_line(stdscr)
            logger.info(f"Input: {line}")

            host.parse_input(line)

            inventory.render(stdscr, host)

    logger.debug(inventory)




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Inventory Scan')
    parser.add_argument('--inventory', required=False, default=pathlib.Path('examples/inventory.yaml'), type=pathlib.Path,
                        help='the path to the inventory file')
    parser.add_argument('--mac-address-field', required=False, type=str, default="bmc_mac",
                        help='the inventory field of mac addresses')
    parser.add_argument('--password-field', required=False, type=str, default="bmc_default_password",
                        help='the password field of mac addresses')
    parser.add_argument('--password-length', required=False, type=int, default=10,
                        help='the length of passwords')
    parser.add_argument('--max-hosts', required=False, type=int, default=10,
                        help='the number of hosts to show onscreen at once')
    parser.add_argument('--mac-seperator', required=False, type=str, default=":",
                        help='the character to use when separating each octet of the mac addresses')

    args = parser.parse_args()

    try:
        curses.wrapper(main, args)
        # main(curses.initscr())
    except KeyboardInterrupt:
        pass